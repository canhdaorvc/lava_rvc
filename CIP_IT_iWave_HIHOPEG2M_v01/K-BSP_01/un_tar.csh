#!/bin/csh -f
#Note: This program run on Linux
#Function: Tar all dir 
#How_to_run: ./un_tar.csh
#Writen: RVC/AnhTran"
#Version: 0.1 First created

clear
#=================Input_seting===============#
setenv current_dir `pwd`

#=================Check_quality===============#
ls | egrep _ >! list
foreach TC (`cat ./list`)
	tar xvf $TC
end
