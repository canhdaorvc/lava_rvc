#!/bin/bash
#Get HDMI resolution
if [ `cat /etc/hostname` == "ebisu" ]
then
        #Get HDMI resolution
        info=`media-ctl -d /dev/media0 --get-v4l2 "'adv748x 0-0070 hdmi':1"`
        # Setup HDMI input
        media-ctl -d /dev/media0 -r
        media-ctl -d /dev/media0 -l "'rcar_csi2 feaa0000.csi2':1 -> 'VIN4 output':0 [1]"
        media-ctl -d /dev/media0 -V "'rcar_csi2 feaa0000.csi2':1 $info"
        media-ctl -d /dev/media0 -V "'adv748x 0-0070 hdmi':1 $info"
fi

if [ `cat /etc/hostname` == "hihope-rzg2n" ]
then
        media-ctl -d /dev/media0 -r
        media-ctl -d /dev/media0 -l "'rcar_csi2 fea80000.csi2':2 -> 'VIN7 output':0 [1]"
        media-ctl -d /dev/media0 -V "'rcar_csi2 fea80000.csi2':2 [fmt:UYVY8_2X8/1280x960 field:none]"
        media-ctl -d /dev/media0 -V "'ov5645 2-003c':0 [fmt:UYVY8_2X8/1280x960 field:none]"
	echo 1 > /sys/module/ov5645/parameters/virtual_channel
fi
if [ `cat /etc/hostname` == "hihope-rzg2m" ]
then
        media-ctl -d /dev/media0 -r
        media-ctl -d /dev/media0 -l "'rcar_csi2 fea80000.csi2':1 -> 'VIN7 output':0 [1]"
        media-ctl -d /dev/media0 -V "'rcar_csi2 fea80000.csi2':1 [fmt:UYVY8_2X8/1280x960 field:none]"
        media-ctl -d /dev/media0 -V "'ov5645 2-003c':0 [fmt:UYVY8_2X8/1280x960 field:none]"
	echo 1 > /sys/module/ov5645/parameters/virtual_channel
fi

