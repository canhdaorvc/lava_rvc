/*
* Project: CIP LAVA IT test
* Test ID: close_001
* Feature: Checking API close of Serial Debug driver
* Sequence: open() -> close()
* Testing level: API user lib
* Test-case type: Normal
* Expected result: OK
* Name: main.c
* Author: RVC/AnhTran (anh.tran.jc@renesas.com)
* Version: v00r01
* Copyright: Renesas
* Target board: Ebisu_EK874_HIHOPEG2M_HIHOPEG2N
* Details_description: Condition: Call close with valid open mode O_RDONLY; valid file descriptor. Expected result = OK
*/

#include <stdio.h>
#include <fcntl.h>	/* File Control Definitions				*/
#include <termios.h>	/* POSIX Terminal Control Definitions			*/
#include <unistd.h>	/* UNIX Standard Definitions				*/
#include <errno.h>	/* ERROR Number Definitions				*/
#include <sys/ioctl.h>

#include <signal.h>
#include <string.h>
#include <stdlib.h>

void segfault_sigaction(int signal, siginfo_t *si, void *arg);

#define _POSIX_SOURCE	1	/* POSIX compliant source */
#define FALSE		0
#define TRUE		1

/* Declare global variable */
int result = -1;

int main (void)
{
	//Segmentation fault control
	struct sigaction sa;
	memset(&sa, 0, sizeof(sigaction));
	sigemptyset(&sa.sa_mask);
	sa.sa_sigaction = segfault_sigaction;
	sa.sa_flags = SA_SIGINFO;
	sigaction(SIGSEGV, &sa, NULL);

	/* Declare local variable */
	int fd;
	/* Initialize variable and assign value for variable */
	
	/* Call API or system call follow describe in PCL */
	fd = open("/dev/ttySC0", O_RDONLY | O_NOCTTY );	/* ttyUSB0 is the FT232 based USB2SERIAL Converter */
														/* O_RDWR, O_RDONLY, O_WRONLY - Read/Write access to serial port */
														/* O_NOCTTY - No terminal will control the process */
														/* Open in blocking mode,read will wait */
													
	/* Close tty device */
	result = close(fd);

	/* Check return value of sequence */
	switch(result) {
	case 0:
		printf ("OK\n");
		break;
	case -1:
		printf ("NG\n");
		break;
	default:
		printf ("Unknown_result\n");
	};
	
	/* Close tty device in case of NG test cases*/
	if (result == -1)
		close(fd);
	
	return 0;
}

void segfault_sigaction(int signal, siginfo_t *si, void *arg)
{
	printf("NG_SF\n");
	exit(0);
}

