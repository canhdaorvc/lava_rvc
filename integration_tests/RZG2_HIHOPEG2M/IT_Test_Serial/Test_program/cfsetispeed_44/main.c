/*
* Project: CIP LAVA IT test
* Test ID: cfsetispeed_44
* Feature: Checking API cfsetispeed of Serial Debug driver
* Sequence: open() -> tcgetattr() -> cfsetispeed()
* Testing level: API user lib
* Test-case type: Abnormal
* Expected result: NG
* Name: main.c
* Author: RVC/AnhTran (anh.tran.jc@renesas.com)
* Version: v00r01
* Copyright: Renesas
* Target board: EK874_HIHOPEG2M_HIHOPEG2N
* Details_description: Condition: Call cfsetispeed with an invalid baud rate value 115200. Expected result = NG
*/

#include <stdio.h>
#include <fcntl.h>	/* File Control Definitions				*/
#include <termios.h>	/* POSIX Terminal Control Definitions			*/
#include <unistd.h>	/* UNIX Standard Definitions				*/
#include <errno.h>	/* ERROR Number Definitions				*/
#include <sys/ioctl.h>
#include <signal.h>

#define _POSIX_SOURCE	1	/* POSIX compliant source */
#define FALSE		0
#define TRUE		1

/* Declare global variable */
int result = -1;

/* Declare signal handler */
void segfault_sigaction(int signal, siginfo_t *si, void *arg);

int main (void)
{
	/* Declare local variable */
	int fd;
	struct termios SerialPortSettings;	/* Create the structure */

	/* This block of code will catch segmentation fault error */
	struct sigaction sa;

	memset(&sa, 0, sizeof(sigaction));
	sigemptyset(&sa.sa_mask);
	
	/* assign exception handler */
	sa.sa_sigaction = segfault_sigaction;
	sa.sa_flags = SA_SIGINFO;

	sigaction(SIGSEGV, &sa, NULL);
	
	/* Call API or system call follow describe in PCL */
	fd = open("/dev/ttySC1", O_RDWR | O_NOCTTY );	/* ttyUSB0 is the FT232 based USB2SERIAL Converter */
							/* O_RDWR, O_RDONLY, O_WRONLY - Read/Write access to serial port */
							/* O_NOCTTY - No terminal will control the process */
							/* Open in blocking mode,read will wait */

	/*------------------ Setting the Attributes of the serial port using termios structure ---------------- */
	tcgetattr(fd, &SerialPortSettings);	/* Get the current attributes of the Serial port */

	/* Setting the Baud rate */
	result = cfsetispeed(&SerialPortSettings, 115200);	/* Set Read Speed as 115200 */

	/* Check return value of sequence */
	switch(result) {
	case 0:
		printf ("OK\n");
		break;
	case -1:
		printf ("NG\n");
		break;
	default:
		printf ("Unknown_result\n");
	};

	/* Close tty device*/
	close(fd);
	return 0;
}

void segfault_sigaction(int signal, siginfo_t *si, void *arg)
{
	printf("NG\n");
	exit(0);
}
