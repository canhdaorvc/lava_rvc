/*
* Project: RZG_IT_CIP_BSP
* Test ID: test_case
* Feature: Checking libusb_try_lock_events system call
* Sequence: libusb_init();libusb_try_lock_events();libusb_unlock_events();libusb_exit();
* Testing level: system call
* Test-case type: Normal
* Expected result: OK
* Name: main.c
* Author: RVC/AnhTran (anh.tran.jc@rvc.renesas.com)
* Version: v00r01
* Copyright: Renesas
* Target board: Ebisu_EK874_HIHOPEG2M_HIHOPEG2N
* Details_description: External device: USB Storage. Condition: Call API libusb_try_lock_events of usblib library with context NULL after libusb_init(). Expected result = OK
*/
#include <stdio.h>
#include <string.h>
#include <signal.h>
#include <libusb.h>
#include <sys/types.h>
#include <stdlib.h>

void segfault_sigaction(int signal, siginfo_t *si, void *arg);

int main(int argc, char *argv[])
{
	int result;
	struct libusb_context *ctx;

	//Segmentation fault control
	struct sigaction sa;
	memset(&sa, 0, sizeof(sigaction));
	sigemptyset(&sa.sa_mask);
	sa.sa_sigaction = segfault_sigaction;
	sa.sa_flags = SA_SIGINFO;
	sigaction(SIGSEGV, &sa, NULL);

	//Main checking item
	libusb_init(&ctx);
	if (result < 0) {
		printf("Error: libusb_init");
		return 0;
	}

	result = libusb_try_lock_events(NULL); 	// Expected_value 0, the lock was obtained successfully 
	libusb_unlock_events(NULL);
	
	libusb_exit(ctx);

	if( result == 0 ) {
		printf ("OK\n");
	} else {
		printf ("NG\n");
	}

	return 0;
}

void segfault_sigaction(int signal, siginfo_t *si, void *arg)
{
	printf("NG_SF\n");
	exit(0);
}


