#!/bin/bash
#Function: Detect Vendor ID of USB, PCI and ethernet device for AVB

if [ -f "board_config.txt" ]
then
        rm -rf board_config.txt
        touch board_config.txt
else
        touch board_config.txt
fi

#USB ID
#usb_id=`lsusb -v | egrep "iManufacturer|^Bus" | sed 'N; s/\n //; P; D' | egrep -v -w "Linux|Standard Microsystems" | awk '{print $6}' | awk -F":" '{print $1}'`
usb_id=`cat /sys/kernel/debug/usb/devices | egrep Vendor | awk '{print $2}' | egrep -v "045b|1d6b|0424" | sed -e 's/Vendor=//g' | sed -n '1p'`
if [ "$usb_id" != "" ]
then
        echo "USB_ID = 0x$usb_id" >> board_config.txt
fi

#PCI ID 
#pci_vendor_id=`lspci -nn | egrep -v "Host bridge .* NEC Corporatio|USB controller .* NEC Corporation|PCI bridge .* Renesas Technology Corp" | tail -1 |  awk -F'[' '{print $NF}' | awk -F']' '{print $1}' |  awk -F':' '{print $1}'`

#if [ "$pci_vendor_id" != "" ]
#then
#	echo "PCI_ID = 0x$pci_vendor_id" >> board_config.txt
#fi

#pci_dev_id=`lspci -nn | egrep -v "Host bridge .* NEC Corporatio|USB controller .* NEC Corporation|PCI bridge .* Renesas Technology Corp" | tail -1 |  awk -F'[' '{print $NF}' | awk -F']' '{print $1}' |  awk -F':' '{print $NF}'`

#if [ "$pci_dev_id" != "" ]
#then
#	echo "PCI_DEVICE_ID = 0x$pci_dev_id" >> board_config.txt
#fi

#AVB/EthernetMAC/PCI_Ethernet Device

num=`ifconfig  | egrep "^eth" | awk '{print $1}' | wc -l`
if [ "$num" == "1" ]
then
	avb_device=`ifconfig  | egrep "^eth" | awk '{print $1}' | sort | sed -n '1p'`
	if [ "$avb_device" != "" ]
	then
	        echo "AVB_DEV = $avb_device" >> board_config.txt
	fi
	        echo "ETH_DEV = dont_exist" >> board_config.txt
fi


if [ "$num" == "2" ]
then
	avb_device=`ifconfig  | egrep "^eth" | awk '{print $1}' | sort | sed -n '2p'`
	if [ "$avb_device" != "" ]
	then
	        echo "AVB_DEV = $avb_device" >> board_config.txt
	fi

	eth_device=`ifconfig  | egrep "^eth" | awk '{print $1}' | sort | sed -n '1p'`
	if [ "$eth_device" != "" ]
	then
	        echo "ETH_DEV = $eth_device" >> board_config.txt
	fi
fi


if [ "$num" == "3" ]
then
	avb_device=`ifconfig  | egrep "^eth" | awk '{print $1}' | sort | sed -n '3p'`
	if [ "$avb_device" != "" ]
	then
	        echo "AVB_DEV = $avb_device" >> board_config.txt
	fi

	eth_device=`ifconfig  | egrep "^eth" | awk '{print $1}' | sort | sed -n '2p'`
	if [ "$eth_device" != "" ]
	then
	        echo "ETH_DEV = $eth_device" >> board_config.txt
	fi
fi
