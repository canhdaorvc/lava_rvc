/*
* Project: RZG_IT_CIP_BSP
* Test ID: test_case
* Feature: Checking libusb_unlock_events system call
* Sequence: libusb_init();libusb_lock_events();libusb_event_handler_active();libusb_unlock_events();libusb_exit();
* Testing level: system call
* Test-case type: Normal
* Expected result: OK
* Name: main.c
* Author: RVC/AnhTran (anh.tran.jc@rvc.renesas.com)
* Version: v00r01
* Copyright: Renesas
* Target board: Ebisu_EK874_HIHOPEG2M_HIHOPEG2N
* Details_description: External device: USB Storage. Condition: Call API libusb_unlock_events of usblib library with context NULL. Expected result = OK
*/
#include <stdio.h>
#include <string.h>
#include <signal.h>
#include <libusb.h>
#include <sys/types.h>
#include <stdlib.h>
#include <errno.h>
#include "get_vendor_ID_and_device.h"

void segfault_sigaction(int signal, siginfo_t *si, void *arg);

int main(int argc, char *argv[])
{
	struct libusb_device_handle *handle;
	int i, result;
	struct libusb_context *ctx;
	struct libusb_device **dev_list;
	struct libusb_device_descriptor desc;
	uint16_t idVendor;
	uint16_t idProduct;

	struct timeval tv;
	tv.tv_sec = 3;
	tv.tv_usec = 0;
	
	//Segmentation fault control
	struct sigaction sa;
	memset(&sa, 0, sizeof(sigaction));
	sigemptyset(&sa.sa_mask);
	sa.sa_sigaction = segfault_sigaction;
	sa.sa_flags = SA_SIGINFO;
	sigaction(SIGSEGV, &sa, NULL);

	//Get Vendor_ID of USB Device from file ./board_config.txt
	int device_vendor_id;
	device_vendor_id = get_Vendor_ID_USB_PCI("USB_ID");
	//Main checking item
	libusb_init(&ctx);
	if (result < 0) {
		printf("Error: libusb_init");
		return 0;
	}
	libusb_set_debug(ctx, 3);

	ssize_t cnt = libusb_get_device_list(ctx, &dev_list);
	if(cnt < 0) {
		printf("Error: libusb_get_device_list");
		return 0;
	}
	for (i = 0; dev_list[i]; ++i) {
		libusb_device *dev = dev_list[i];
		libusb_get_device_descriptor(dev, &desc);
		if(desc.idVendor == device_vendor_id) {
			idVendor = desc.idVendor;
			idProduct = desc.idProduct;
		}
	struct libusb_config_descriptor *config;
	libusb_get_config_descriptor(dev, 0, &config);
	libusb_free_config_descriptor(config);
	}

	handle = libusb_open_device_with_vid_pid(ctx, idVendor, idProduct);
	if(handle == NULL)
	{
		printf("Error: libusb_open_device_with_vid_pid\n");
		return 0;
	}

	libusb_free_device_list(dev_list, 1);

	// check function libusb_unlock_events
	libusb_lock_events(ctx);			// ensure that only one thread is monitoring libusb event sources at any one time.
	result = libusb_event_handler_active(ctx);	// result = 1 if a thread is handling events
	if(result != 1){
		printf("Error: libusb_lock_events \n");
		return 0;
	}
	else{
	libusb_unlock_events(NULL);			// Release the lock.	
		 result = libusb_event_handler_active(ctx); 	// result = 0 if no threads currently handling events
	}

	libusb_reset_device(handle);
	libusb_close(handle);
	libusb_exit(ctx);
Exit:
	if( result == 0) {
		printf ("OK\n");
	} else {
		printf ("NG\n");
	}
	printf("Error: %s\n", strerror(errno));
	return 0;
}

void segfault_sigaction(int signal, siginfo_t *si, void *arg)
{
	printf("NG_SF\n");
	exit(0);
}


