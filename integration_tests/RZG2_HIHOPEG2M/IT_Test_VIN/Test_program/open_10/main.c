/*
* Project: CIP LAVA IT test
* Test ID: open_10
* Feature: Checking system call open of VIN driver
* Sequence: open()
* Testing level: API user lib
* Test-case type: Normal
* Expected result: OK
* Name: main.c
* Author: RVC/AnhTran (anh.tran.jc@renesas.com)
* Version: v00r01
* Copyright: Renesas
* Target board: HIHOPEG2M_HIHOPEG2N
* Details_description: Condition: Call open device /dev/video2 with supported device and supported mode. Expected result = OK
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <linux/videodev2.h>
#include <sys/ioctl.h>


#include <signal.h>
#include <string.h>

void segfault_sigaction(int signal, siginfo_t *si, void *arg);

/* Declare global variable */
int result = -1;

int main(int argc, char *argv[])
{
	//Segmentation fault control
	struct sigaction sa;
	memset(&sa, 0, sizeof(sigaction));
	sigemptyset(&sa.sa_mask);
	sa.sa_sigaction = segfault_sigaction;
	sa.sa_flags = SA_SIGINFO;
	sigaction(SIGSEGV, &sa, NULL);

	/* Declare local variable */

	/* Initialize variable and assign value for variable */

	/* Call API or system call follow describe in PCL */
	result = open("/dev/video2", O_RDWR);

	/* Check return value of sequence */
	if (result >= 0) {
		printf ("OK\n");
	}
	else if (result == -1) {
		printf ("NG\n");
	}
	else {
		printf ("Unknown_result\n");
	}
	
	/* Close the device */
	close(result);
	return 0;
}

void segfault_sigaction(int signal, siginfo_t *si, void *arg)
{
	printf("NG_SF\n");
	exit(0);
}

