/*
* Project: CIP LAVA IT test
* Test ID: ioctl_VIDIOC_QUERYBUF_11
* Feature: Checking system call ioctl_VIDIOC_QUERYBUF of VIN driver
* Sequence: open() -> ioctl_VIDIOC_S_FMT() -> ioctl_VIDIOC_REQBUFS -> ioctl_VIDIOC_QUERYBUF()
* Testing level: API user lib
* Test-case type: Normal
* Expected result: OK
* Name: main.c
* Author: RVC/AnhTran (anh.tran.jc@renesas.com)
* Version: v00r01
* Copyright: Renesas
* Target board: HIHOPEG2M_HIHOPEG2N
* Details_description: Condition: Call ioctl_VIDIOC_QUERYBUF with device /dev/video0 to query the allocating status of each of 2 buffers. Expected result = OK
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <linux/videodev2.h>
#include <sys/ioctl.h>


#include <signal.h>
#include <string.h>

void segfault_sigaction(int signal, siginfo_t *si, void *arg);

/* Declare global variable */
int result = -1;

int main(int argc, char *argv[])
{
	//Segmentation fault control
	struct sigaction sa;
	memset(&sa, 0, sizeof(sigaction));
	sigemptyset(&sa.sa_mask);
	sa.sa_sigaction = segfault_sigaction;
	sa.sa_flags = SA_SIGINFO;
	sigaction(SIGSEGV, &sa, NULL);

	/* Declare local variable */
	int fd;
	struct v4l2_format format;
	struct v4l2_requestbuffers req_buf;
	struct v4l2_buffer buf;
	int i;
	
	/* Initialize variable and assign value for variable */
	memset (&format, 0, sizeof(format));
	
	/* Call API or system call follow describe in PCL */
	fd = open("/dev/video0", O_RDWR);
	
	/* Setting scaling & format */
	format.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	format.fmt.pix.width			= 640;
	format.fmt.pix.height			= 480;
	format.fmt.pix.pixelformat		= V4L2_PIX_FMT_RGB565;
	format.fmt.pix.field			= V4L2_FIELD_INTERLACED;

	ioctl(fd, VIDIOC_S_FMT, &format);
	
	/* Request allocated buffers */
	req_buf.type				= V4L2_BUF_TYPE_VIDEO_CAPTURE;
	req_buf.memory				= V4L2_MEMORY_MMAP;
	req_buf.count				= 2;
	
	ioctl(fd, VIDIOC_REQBUFS, &req_buf);
	
	/* Query buffers */
	for(i = 0; i < req_buf.count; i++)
	{
		memset (&buf, 0, sizeof(buf));
		buf.type		= V4L2_BUF_TYPE_VIDEO_CAPTURE;
		buf.memory		= V4L2_MEMORY_MMAP;
		buf.index		= i;

		result = ioctl(fd, VIDIOC_QUERYBUF, &buf);
		if (result == -1) {
			goto exit;
		}
	}
	
exit:
	/* Check return value of sequence */
	switch(result) {
	case 0:
		printf ("OK\n");
		break;
	case -1:
		printf ("NG\n");
		break;
	default:
		printf ("Unknown_result\n");
	};
	
	/* Close the device */
	close(fd);
	return 0;
}

void segfault_sigaction(int signal, siginfo_t *si, void *arg)
{
	printf("NG_SF\n");
	exit(0);
}

